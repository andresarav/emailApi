EmailJs, es una librería que sirve para enviar emails sin la necesidad de un servidor,
es un API que nos permite hacer este proceso desde el cliente con una llamada HTTP post


<!-- Paso #1 Conectar a nuestra cuenta el servicio de Gmail -->
Email Sevices/Add New Service/Elije servicio(Gmail)

<!-- Paso #2 Crear una plantilla de correo -->
Email Templates/Create new Template/nombre template

<!--
1.to email: aqui definimos el email donde queremos que lleguen los correos generados
2.From Name: Asunto, titulo del correo que aparecerá en la bandeja de entrada -->

para el siguiente paso necesitaremos tres keys:

User ID se encuentra en la opción Account > User info de tu Dashboard
Service ID se encuentra en la opción Email Services y eligiendo el servicio
Template ID se encuentra en la opción Email Templates y eligiendo la plantilla


en tu codigo javascript:

<!-- forma 1 -->
    emailjs.init("<YOUR USER ID>");

    enviar(){
         let data = {
             from_name: this.from_name,
             from_email: this.from_email,
             message: this.message,
             subject: this.subject,
         };

         emailjs.send("<YOUR SERVICE ID>","<YOUR TEMPLATE ID>", data)
         .then(function(response) {
             if(response.text === 'OK'){
                 alert('El correo se ha enviado de forma exitosa');
             }
            console.log("SUCCESS. status=%d, text=%s", response.status, response.text);
         }, function(err) {
             alert('Ocurrió un problema al enviar el correo');
            console.log("FAILED. error=", err);
         });
       }

<!-- forma 2 Metodo post http-->

        var data = {
            service_id: '<YOUR SERVICE ID>',
            template_id: '<YOUR TEMPLATE ID>',
            user_id: '<YOUR USER ID>',
            template_params: {
                'username': 'James',
                'g-recaptcha-response': '03AHJ_ASjnLA214KSNKFJAK12sfKASfehbmfd...'
            }
        };

        $.ajax('https://api.emailjs.com/api/v1.0/email/send', {
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json'
        }).done(function() {
            alert('Your mail is sent!');
        }).fail(function(error) {
            alert('Oops... ' + JSON.stringify(error));
        });




.
